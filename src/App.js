import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import Popular from "./components/Popular";
import Battle from "./components/Battle";
import "./App.css";
import ResetPage from "./components/ResetPage";
import Header from "./components/Header";
import Submission from "./components/Submission";
import ErrorPage from "./components/ErrorPage";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      language: "All",
      light: true,
      player1: "",
      player2: "",
    };
  }
  themeChange = (data) => {
    this.setState({
      light: !data,
    });
  };
  battleHandler = (firstName, lastName) => {
    this.setState({
      player1: firstName,
      player2: lastName,
    });
  };

  render() {
    return (
      <div className={`${this.state.light}`}>
        <Header lightChanger={this.themeChange} />

        <Switch>
          <Route exact path="/">
            <Popular />
          </Route>
          <Route exact path="/battle">
            <Battle />
          </Route>
          <Route
            exact
            path={`/battle/:player1&:player2`}
            render={(path) => <Submission pathname={path} />}
          />

          <Route>
            <ErrorPage />
          </Route>
        </Switch>
      </div>
    );
  }
}
