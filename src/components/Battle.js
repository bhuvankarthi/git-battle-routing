import React, { Component } from "react";
import fetch from "node-fetch";
import Header from "./Header";
import { HiUsers } from "react-icons/hi";
import { FaFighterJet } from "react-icons/fa";
import { GiTrophy } from "react-icons/gi";
import "../css/Battle.css";
import { Link } from "react-router-dom";

export default class extends Component {
  constructor() {
    super();
    this.state = {
      stage: 1,
      player1: "",
      player2: "",
      player1Status: true,
      player2Status: true,
      player1Data: "",
      player2Data: "",
      count1: 1,
      count2: 1,
      invalidUser1: false,
      invalidUser2: false,
    };
  }

  data = async (playerName) => {
    const userData = await fetch(`https://api.github.com/users/${playerName}`);

    const jsonData = await userData.json();
    console.log(jsonData);
    return jsonData;
  };

  player1Submit = async () => {
    if (this.state.player1 !== "") {
      this.setState({
        player1Status: false,
        count1: 1,
      });
      let dataOfUser = await this.data(this.state.player1);
      if (!Object.keys(dataOfUser).includes("message")) {
        this.setState({
          player1Data: dataOfUser,
          player1Status: !this.state.player1Status,
          count1: this.state.count1 + 1,
          invalidUser1: false,
        });
      } else {
        this.setState({
          player1Status: true,
          invalidUser1: true,
        });
      }
    } else {
      this.setState({
        player1Status: false,
        count1: this.state.count1 + 1,
      });
    }
  };

  player2Submit = async () => {
    if (this.state.player2 !== "") {
      this.setState({
        player2Status: false,
        count2: 1,
      });
      let dataOfUser = await this.data(this.state.player2);
      if (!Object.keys(dataOfUser).includes("message")) {
        this.setState({
          player2Data: dataOfUser,
          player2Status: !this.state.player2Status,
          count2: this.state.count2 + 2,
          invalidUser2: false,
        });
      } else {
        this.setState({
          invalidUser2: true,
          player2Status: true,
        });
      }
    } else {
      this.setState({
        player2Status: false,
        count2: this.state.count2 + 2,
      });
    }
  };

  input1Change = (e) => {
    this.setState({
      player1: e.target.value,
    });
  };

  input2Change = (e) => {
    this.setState({
      player2: e.target.value,
    });
  };
  battleHandler = (firstData, lastData) => {
    this.props.finalPage(firstData, lastData);
  };

  render() {
    console.log(this.state.player1Status);
    return (
      <div>
        <div className="battle-total-div">
          <h1>{"Instuctions"}</h1>
          <section className="instructions-section">
            <div>
              <p className="instruction-titles">Enter two Github users</p>
              <div className="users-icon">
                <HiUsers />
              </div>
            </div>
            <div>
              <p className="instruction-titles">Battle</p>
              <div className="plane-icon">
                <FaFighterJet />
              </div>
            </div>
            <div>
              <p className="instruction-titles">see the winner</p>
              <div className="trophy-icon">
                <GiTrophy />
              </div>
            </div>
          </section>
          <div class="compete-div">
            <p class="players-para">Players</p>
            <div class="players-entry">
              <div className="player-one">
                <label>
                  Player One
                  <input
                    type="textbox"
                    placeholder="github username"
                    className="players-input"
                    onChange={(e) => this.input1Change(e)}
                  />
                  {!this.state.player1Status && this.state.count1 > 1 && (
                    <div className="alert-message">player1 Not Found</div>
                  )}
                  {this.state.invalidUser1 && (
                    <div className="alert-message">player1 Not Found</div>
                  )}
                  {this.state.player1Status &&
                    this.state.count1 > 1 &&
                    !this.state.invalidUser1 && (
                      <div class="player-found">
                        <img
                          src={this.state.player1Data.avatar_url}
                          className="player1-found"
                        />
                        <p>{this.state.player1Data.name}</p>
                      </div>
                    )}
                </label>
                <button className="submit" onClick={() => this.player1Submit()}>
                  SUBMIT
                </button>
              </div>

              <div className="player-two">
                <label>
                  Player Two
                  <input
                    type="textbox"
                    placeholder="github username"
                    className="players-input"
                    onChange={(e) => this.input2Change(e)}
                  />
                  {!this.state.player2Status && this.state.count2 > 1 && (
                    <div className="alert-message">player2 Not Found</div>
                  )}
                  {this.state.invalidUser2 && (
                    <div className="alert-message">player2 Not Found</div>
                  )}
                  {this.state.player2Status &&
                    this.state.count2 > 2 &&
                    !this.state.invalidUser2 && (
                      <div class="player-found">
                        <img
                          src={this.state.player2Data.avatar_url}
                          className="player2-found"
                        />
                        <p>{this.state.player2Data.name}</p>
                      </div>
                    )}
                </label>
                <button className="submit" onClick={() => this.player2Submit()}>
                  SUBMIT
                </button>
              </div>
            </div>
            {this.state.player1Data && this.state.player2Data && (
              <div>
                <Link
                  to={`/battle/${this.state.player1Data.login}&${this.state.player2Data.login}`}
                >
                  <button
                    className="Battle-button"
                    onClick={() =>
                      this.props.file(
                        this.state.player1Data.login,
                        this.state.player2Data.login
                      )
                    }
                  >
                    Battle
                  </button>
                </Link>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
