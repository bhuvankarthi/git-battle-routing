import React, { Component } from "react";
import { FaLocationArrow } from "react-icons/fa";
import { BiCodeAlt } from "react-icons/bi";
import { IoMdPeople } from "react-icons/io";
import { IoIosPeople } from "react-icons/io";
import { BsFillPersonFill } from "react-icons/bs";
import "../css/Result.css";

export default class ResultComponents extends Component {
  render() {
    return (
      <section>
        <img
          src={this.props.obj.picture}
          alt={`${this.props.obj.name} image`}
          className="result-pic"
        />
        <h3 className="result-name">{this.props.obj.login}</h3>
        <p className="score-para">{`score : ${this.props.obj.score}`}</p>

        <div className="content-result-component">
          <div className="icons-content">
            <BsFillPersonFill className="person-icon" />
            <p>{`${this.props.obj.name}`}</p>
          </div>
          {this.props.obj.location !== null && (
            <div className="icons-content">
              <FaLocationArrow className="location-icon" />
              <p>{this.props.obj.location}</p>
            </div>
          )}

          <div className="icons-content">
            <IoIosPeople className="followers-icon" />
            <p>{`${this.props.obj.followers} Followers`}</p>
          </div>
          <div className="icons-content">
            <IoMdPeople className="following-icon" />
            <p>{`${this.props.obj.following} Following`}</p>
          </div>

          <div className="icons-content">
            <BiCodeAlt className="repo-icon" />
            <p>{`${this.props.obj.publicRepos} Repositories`}</p>
          </div>
        </div>
      </section>
    );
  }
}
