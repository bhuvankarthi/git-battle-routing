import React, { Component } from "react";
import { Link } from "react-router-dom";
import fetch from "node-fetch";
import ResultComponents from "./ResultComponents";
import "../css/Result.css";

export default class Submission extends Component {
  constructor() {
    super();
    this.state = {
      player1Data: null,
      player2Data: null,
    };
  }

  getData = async () => {
    let { player1, player2 } = this.props.pathname.match.params;

    let data1 = await fetch(`https://api.github.com/users/${player1}`);
    let data2 = await fetch(`https://api.github.com/users/${player2}`);
    let jsonData1 = await data1.json();
    let jsonData2 = await data2.json();

    let obj1 = {
      picture: jsonData1.avatar_url,
      score: jsonData1.public_repos * 5,
      login: jsonData1.login,
      name: jsonData1.name,
      location: jsonData1.location,
      followers: jsonData1.followers,
      following: jsonData1.following,
      publicRepos: jsonData1.public_repos,
    };
    let obj2 = {
      picture: jsonData2.avatar_url,
      score: jsonData2.public_repos * 5,
      login: jsonData2.login,
      name: jsonData2.name,
      location: jsonData2.location,
      followers: jsonData2.followers,
      following: jsonData2.following,
      publicRepos: jsonData2.public_repos,
    };

    this.setState({
      player1Data: obj1,
      player2Data: obj2,
    });
  };

  componentDidMount() {
    this.getData();
  }
  render() {
    return (
      <div>
        {this.state.player1Data === null && this.state.player2Data === null ? (
          <div>Data is being fetched</div>
        ) : (
          ""
        )}
        {this.state.player1Data !== null && this.state.player1Data !== null ? (
          <main>
            <div className="total-result-component">
              <div className="result-component">
                <div className="final-result-heading">
                  {this.state.player1Data.score >
                    this.state.player2Data.score &&
                  this.state.player1Data.score !== this.state.player2Data.score
                    ? "Winner"
                    : this.state.player1Data.score ===
                      this.state.player2Data.score
                    ? "Tie"
                    : "Loser"}
                </div>

                <ResultComponents obj={this.state.player1Data} />
              </div>
              <div className="result-component">
                <div className="final-result-heading">
                  {this.state.player1Data.score <
                    this.state.player2Data.score &&
                  this.state.player1Data.score !== this.state.player2Data.score
                    ? "Winner"
                    : this.state.player1Data.score ===
                      this.state.player2Data.score
                    ? "Tie"
                    : "Loser"}
                </div>
                <ResultComponents obj={this.state.player2Data} />
              </div>
            </div>
            <Link to={"/battle"}>
              <button className="reset-button">Reset</button>
            </Link>
          </main>
        ) : (
          ""
        )}
      </div>
    );
  }
}
