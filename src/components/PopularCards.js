import React, { Component } from "react";
import { FaUserAlt } from "react-icons/fa";
import { RiStarSFill } from "react-icons/ri";
import { VscGitMerge } from "react-icons/vsc";
import { BsFillExclamationTriangleFill } from "react-icons/bs";

import "../css/PopularCards.css";

export default class PopularCards extends Component {
  constructor(props) {
    super(props);
  }

  obj = {
    avatarUrl: this.props.name.owner.avatar_url,
    name: this.props.name.name,
    UserName: this.props.name.owner.login,
    githubApi: this.props.name.archive_url,
    stargazersCount: this.props.name.stargazers_count,
    forksCount: this.props.name.forks,
    openIssues: this.props.name.open_issues_count,
  };

  render() {
    const user = this.props.name;
    const id = this.props.id + 1;
    return (
      <div className="cards-div">
        <div className="total-elements">
          <div className="heading">
            <h1 className="ranking">{`#${id}`}</h1>
            <img
              src={this.obj.avatarUrl}
              alt={`${this.obj.UserName} image`}
              className="logos"
            />
          </div>
          <div className="userName">
            <h2>{this.obj.UserName}</h2>
          </div>

          <p className="card-elements">
            <FaUserAlt className="icons-person" />

            <a href={this.obj.githubApi}>{this.obj.UserName}</a>
          </p>
          <p className="card-elements">
            <RiStarSFill className="icons-stars" />
            {`${this.obj.stargazersCount} stars`}
          </p>
          <p className="card-elements">
            <VscGitMerge className="icons-forks" />
            {`${this.obj.forksCount} forks`}
          </p>
          <p className="card-elements">
            <BsFillExclamationTriangleFill className="icons-warning" />
            {`${this.obj.openIssues} issues`}
          </p>
        </div>
      </div>
    );
  }
}
