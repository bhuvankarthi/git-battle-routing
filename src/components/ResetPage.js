import React, { Component } from "react";
import Battle from "./Battle";
import Submission from "./Submission";

export default class ResetPage extends Component {
  constructor() {
    super();
    this.state = {
      stage: 1,
    };
  }
  battleHandler = (fd, ld) => {
    console.log(fd);
  };
  render() {
    return (
      <>
        {this.state.stage === 1 && <Battle finalPage={this.battleHandler} />}
        {this.state.stage === 2 && <Submission />}
      </>
    );
  }
}
