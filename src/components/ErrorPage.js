import React, { Component } from "react";
import error from "../pictures/error.jpg";
import { Link } from "react-router-dom";
import "../css/Result.css";
export default class extends Component {
  render() {
    return (
      <div className="error-div">
        <img src={error} alt="error image" className="error-image" />
        <Link to="/">
          <button className="back-to-home">Home Page</button>
        </Link>
      </div>
    );
  }
}
